import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

/* class based component
 class Square extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            value: null
        }
    }
    render() {
      return (
        <button 
        className="square" 
        onClick={ () => this.props.onClick()}>
          {this.props.value}  
        </button>
      );
    }
  }
 */
/* function based component */
function Square(props){
    return (
        <button 
            className="square" 
            onClick={ () => props.onClick()}>
            {props.value}  
        </button>
      );
}

  
  class Board extends React.Component {


    // constructor(props){
    //     super(props)
    //     this.state = {
    //         squares : Array(9).fill(null),
    //         isXNext :true
    //     }
    // }


    renderSquare(i) {
      return ( 
      <Square 
        value={this.props.squares[i]} 
        onClick={()=>this.props.onClick(i)}
      />
      );
    }
    
    render() {

      return (
        <div>
          {/* <div className="status">{status}</div> */}
          <div className="board-row">
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}
          </div>
          <div className="board-row">
            {this.renderSquare(3)}
            {this.renderSquare(4)}
            {this.renderSquare(5)}
          </div>
          <div className="board-row">
            {this.renderSquare(6)}
            {this.renderSquare(7)}
            {this.renderSquare(8)}
          </div>
        </div>
      );
    }
  }
  
  class Game extends React.Component {
      constructor(props){
          super(props)
          this.state = {
              history :[
                  Array(9).fill(null)
              ],
          }
      }
    resetBord(idx){
      console.log(idx, this.state.history)
      let newHistoy = this.state.history.slice()
      newHistoy.splice(idx, newHistoy.length - idx)
      console.log(newHistoy)
         this.setState({
             history: newHistoy
         });
    }
    handleClick(i){
        let squares = this.state.history[this.state.history.length-1].slice()
        if(calculateWinner(squares) || squares[i]){
            return
        }
         let newHistoy = this.state.history.slice()
         squares[i] = newHistoy.length%2 ?"O":"X";
         newHistoy.push(squares)
         this.setState({
             history: newHistoy
         });
     }
    render() {
      let status = 'Next player: '+(this.state.history.length%2? "O" : "X");
      let winner = calculateWinner(this.state.history[this.state.history.length-1])
     if (winner !== null){
         status = "Winner is "+winner
     }

     let moves = this.state.history.map((move, idx)=>{
       if(idx){
        return <li key={idx}><button  type="button" onClick={()=>this.resetBord(idx)}> {idx} </button> </li>
       }
       return "Game started"
     })
     
      return (
        <div className="game">
          <div className="game-board">
            <Board  
            squares = {
                this.state.history[this.state.history.length-1]
            }
            onClick = {(i)=>this.handleClick(i)}
            />
          </div>
          <div className="game-info">
            <div>{status}</div>

            <ol>{moves}</ol>
          </div>
        </div>
      );
    }
  }
  
  // ========================================
  
  ReactDOM.render(
    <Game />,
    document.getElementById('root')
  );
  
  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
      }
    }
    return null;
  }